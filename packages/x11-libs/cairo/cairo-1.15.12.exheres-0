# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'cairo-1.6.4.ebuild' from Gentoo which is:
#     Copyright 1999-2008 Gentoo Foundation

require cairo autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]

SLOT="0"
PLATFORMS="~amd64 ~arm ~x86"
MYOPTIONS="X"

# known to be broken upstream
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        X? (
            x11-proto/xorgproto
        )
    build+run:
        dev-libs/glib:2[>=2.14]
        media-libs/fontconfig[>=2.2.95]
        media-libs/freetype:2[>=2.1.9]
        media-libs/libpng:=[>=1.2.8]
        x11-libs/pixman:1[>=0.30.0]
        X? (
            x11-dri/mesa[X]
            x11-libs/libX11
            x11-libs/libxcb[>=1.6]
            x11-libs/libXext
            x11-libs/libXrender[>=0.6]
        )
   test:
       app-text/ghostscript [[ note = [ required for ps ] ]]
       app-text/libspectre [[ note = [ required for building ps tests ] ]]
       app-text/poppler[glib][>=0.17.4] [[ note = [ required for building pdf tests ] ]]
       gnome-desktop/librsvg[>=2.35.0] [[ note = [ required for building svg tests ] ]]
"

# Be careful when enabling expermental or unsupported backends
#   - Hard disable unsupported xlib xcb backend
#     This backend is incomplete and causes rendering problems
#   - The experimental drm backend fails to build
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-fc
    --enable-ft
    --enable-gobject
    --enable-pdf
    --enable-png
    --enable-ps
    --enable-svg
    --enable-tee
    --disable-directfb
    --disable-drm
    --disable-qt
    --disable-static
    --disable-vg
    --disable-wgl
    --disable-xlib-xcb

    ac_cv_lib_lzo2_lzo2a_decompress=no # Disable automagic dep on lzo
    ax_cv_c_float_words_bigendian=no  # TODO(compnerd) use a compiler check here
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'X egl'
    'X gl'
    'X xcb'
    'X xcb-shm'
    'X xlib'
    'X xlib-xrender'
)

DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-full-testing'
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( BIBLIOGRAPHY CODING_STYLE KNOWN_ISSUES PORTING_GUIDE RELEASING )

AT_M4DIR=( 'build' )

src_prepare() {
    # using 'check_PROGRAMS' tests will be compiled on `make check` and not during `make`
    edo sed -e 's/noinst_PROGRAMS/check_PROGRAMS/' \
            -i test/Makefile.am

    # TODO: Upstream cross/multiarch fix
    edo sed -e "s/if strings/if $(exhost --tool-prefix)strings/" \
            -i build/aclocal.float.m4

    # Don't use pkg-config directly
    edo sed -e "s/pkg-config/${PKG_CONFIG}/" \
        -i configure.ac

    autotools_src_prepare
}

