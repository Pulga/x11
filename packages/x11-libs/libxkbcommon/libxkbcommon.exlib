# Copyright 2012 Alex Elsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=xkbcommon ] meson

SUMMARY="A common input handling library"
HOMEPAGE+=" https://xkbcommon.org"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="X doc wayland"

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex
        virtual/pkg-config[>=0.9.0]
        x11-proto/xorgproto
        doc? ( app-doc/doxygen )
    build+run:
        X? ( x11-libs/libxcb[>=1.10] )
        wayland? (
            sys-libs/wayland[>=1.2.0]
            sys-libs/wayland-protocols[>=1.7]
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dxkb-config-root=/usr/share/X11/xkb
)
MESON_SRC_CONFIGURE_OPTION_ENABLES=(
    'X x11'
    'doc docs'
    wayland
)

src_prepare() {
    # fix build, last checked: 0.8.2
    edo sed \
        -e '/fuzz-/d' \
        -i "${MESON_SOURCE}"/meson.build

    default
}

